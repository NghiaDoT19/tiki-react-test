import React, { Component } from 'react';
import axios from 'axios';

class ProductDetail extends Component {
    constructor(props) {
        super(props);
        const match = props.match;

        this.state={
            productItem: []
        }

        this.getProductItem(match);
    }

    getProductItem(match) {
        axios
            .get(`http://localhost:3001/products/${match.params.productId}`)
            .then((result) => {
                this.setState({
                    productItem:result.data
                });
            })
    }

    render() {
        const data = this.state.productItem;
        return(
            <div className="product-detail">
                <h2 className="product-detail__name">{data.title}</h2>
                <div className="row">
                    <div className="col-md-6">
                        <img className="product-detail__image" src={data.image} />
                    </div>
                    <div className="col-md-6">
                        <p className="product-detail__price">{data.price}</p>
                        <p className="product-detail__description">{data.description}</p>
                    </div>
                </div>
            </div>
        )
    }
}

export default ProductDetail;