import React, { Component } from 'react';
import { Link, Route } from 'react-router-dom';
import axios from 'axios';

class Product extends Component {
    constructor(props) {
        super(props);

        this.state={
            products: [],
            loading: true
        }

        this.getData();
    }

    getData() {
        axios
            .get("http://localhost:3001/products")
            .then((result) => {
                this.setState({
                    products:result.data,
                    loading: false
                });
            });
    }

    render() {
        const props = this.props;
        const match = props.match;
        const productData = this.state.products.map((product) => {
            return (
                <li key={product.id} className="col-xs-6 col-md-4 col-lg-3 product__item">
                    <Link to={`/product/${product.id}`}>
                        <div className="product__inner">
                            <img className="product__image" src={product.image} />
                            <p className="product__name">{product.title}</p>
                            <p className="product__price">{product.price}</p>
                        </div>
                    </Link>
                </li>
            )
        })

        if (this.state.loading) {
            return (
                <div>Loading...</div>
            )
        } else {
            return (
                <ul className="product d-flex flex-wrap">
                    {productData}
                </ul>
            );
        }
    }
};

export default Product;