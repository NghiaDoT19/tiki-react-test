import React, { Component } from 'react';
import { Redirect, Link, Route, Switch } from "react-router-dom";
import Product from './product';
import ProductDetail from './product-detail';
import './../css/App.css';

class App extends Component {
  render() {
    return (
      <div>
          <Route exact path="/" component={Product}/>
          <Route path="/product/:productId" component={ProductDetail}/>
      </div>
    );
  }
}

export default App;
